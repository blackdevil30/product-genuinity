const {
    Message,
    EventFilter,
    EventList,
    EventSubscription,
    ClientEventsSubscribeRequest,
    ClientEventsSubscribeResponse
  } = require('sawtooth-sdk/protobuf');
const {TextDecoder} = require('text-encoding/lib/encoding');
const { Stream } = require('sawtooth-sdk/messaging/stream');
var decoder = new TextDecoder('utf8');
const VALIDATOR_URL = "tcp://validator:4004";


//returns the message data as a list 
function getEventsMessage(message){
    let Eventlist =  EventList.decode(message.content).events;
    Eventlist.map(function(event){
        if(event.eventType === 'sawtooth/block-commit'){
            console.log("\n \n \n");
            console.log("Block commit event found   : ", event);
    }

    if(event.eventType === 'product genuinity/ProductRegisteredToDealer'){
        console.log("\n \n \n");
        console.log("product registered to dealer event    : ", event);
    }
    if(event.eventType === 'product genuinity/ProductRegisteredToCustomer'){
        console.log("\n \n \n");
        console.log("product registered to customer event    : ", event);
    }
    if(event.eventType === "product genuinity/ProductDeleted"){
        console.log("\n\n\n");
        console.log("product deleted by manufacturer : ", event);
    }
});
}

// returns the subscription request status 
function checkStatus(response){
    let msg = "";
    if (response.status === 0){
            msg = 'subscription : OK';
    }if (response.status === 1){
            msg = 'subscription : GOOD ';
    }else{
            msg= 'subscription failed !';}
    return msg;
}


function EventSubscribe(URL){
    let stream = new Stream(URL);
    //Creating a block-commit event subscription
    const blockCommitSubscription  = EventSubscription.create({
            eventType: 'sawtooth/block-commit'
    });
    const registrationDealer = EventSubscription.create({
            eventType: 'product genuinity/ProductRegisteredToDealer'
    });
    const registrationCustomer = EventSubscription.create({
            eventType: 'product genuinity/ProductRegisteredToCustomer'
    });

    const productRemoveManu = EventSubscription.create({
            eventType: 'product genuinity/ProductDeleted'
    });

 //creating a subscription_request
 const subscription_request = ClientEventsSubscribeRequest.encode({
    subscriptions : [blockCommitSubscription, registrationDealer, registrationCustomer, productRemoveManu]
}).finish();

stream.connect(() => {
    stream.send(Message.MessageType.CLIENT_EVENTS_SUBSCRIBE_REQUEST,subscription_request)
    .then(function (response){
       return ClientEventsSubscribeResponse.decode(response) ;
    })
    .then(function (decoded_Response){
       console.log(checkStatus(decoded_Response));
    })
    
    stream.onReceive(getEventsMessage);
});

}


EventSubscribe(VALIDATOR_URL);