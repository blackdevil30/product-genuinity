const fetch = require('node-fetch');
const crypto = require('crypto');
const {CryptoFactory, createContext } = require('sawtooth-sdk/signing')
const protobuf = require('sawtooth-sdk/protobuf')
const {Secp256k1PrivateKey} = require('sawtooth-sdk/signing/secp256k1') 
const {TextEncoder} = require('text-encoding/lib/encoding')
var encoder =new TextEncoder('utf8');




// FAMILY NAME
FAMILY_NAME = 'product genuinity';


// function to Hash Data
function hash(data){
return crypto.createHash('sha512').update(data).digest('hex');
}




 /* function to create Transaction 
parameter : 
familyName -  the transaction family name 
inputlist - list of input addressess
outputlist - list of output addressess
privkey - the user private key
payload - payload
familyVersion - the version of the family
*/

  function createTransaction(familyName,inputList,outputList,key,payload,familyVersion = '1.0'){

    const privateKeyHex = key;
    const context = createContext('secp256k1');
    const secp256k1pk = Secp256k1PrivateKey.fromHex(privateKeyHex.trim());
    signer = new CryptoFactory(context).newSigner(secp256k1pk);
    const payloadBytes = encoder.encode(payload);
   
    //create transaction header
    const transactionHeaderBytes = protobuf.TransactionHeader.encode({
      familyName: familyName,
      familyVersion: familyVersion,
      inputs: inputList,
      outputs: outputList,
      signerPublicKey: signer.getPublicKey().asHex(),
      nonce: "" + Math.random(),
      batcherPublicKey: signer.getPublicKey().asHex(),
      dependencies: [],
      payloadSha512: hash(payloadBytes),
    }).finish();
    
    // create transaction
    const transaction = protobuf.Transaction.create({
      header: transactionHeaderBytes,
      headerSignature: signer.sign(transactionHeaderBytes),
      payload: payloadBytes
    });
    const transactions = [transaction];
    
    //create batch header
    const  batchHeaderBytes = protobuf.BatchHeader.encode({
      signerPublicKey: signer.getPublicKey().asHex(),
      transactionIds: transactions.map((txn) => txn.headerSignature),
    }).finish();
  
    const batchSignature = signer.sign(batchHeaderBytes);
   
    //create batch 
    const batch = protobuf.Batch.create({
      header: batchHeaderBytes,
      headerSignature: batchSignature,
      transactions: transactions,
    });
    
    //create batchlist
    const batchListBytes = protobuf.BatchList.encode({
      batches: [batch]
    }).finish();
  
    sendTransaction(batchListBytes);	
  }
  
  /*
  function to submit the batchListBytes to validator
  */
  async function sendTransaction(batchListBytes){
    
     let resp =await fetch('http://rest-api:8008/batches', {
        method: 'POST',
        headers: { 'Content-Type': 'application/octet-stream'},
        body: batchListBytes
        });
           console.log("response", resp);
  }






class Product {




/*
function to generate address

*used for different unique  identifier hased it and stored in addressIdHash, addressIdHash2,addressIdHas3,addressIdHash4
nameHash- hash value of the family name 
pHash - hash value of product name
pIdHash - hash value of the product unoque Identifictaion number
*/ 
getProductAddress(productR, pId){

let addressIdHash= hash("product genuinity register is to");
let addressIdHash2 = hash("track and to verify the genuinity of the product");
let addressIdHash3 = hash("with the use of blockchain");
let addressIdHash4 = hash("implementation using hyperledger sawtooth");

  let nameHash = hash("product genuinity");
  let pHash = hash(productR);
  let pIdHash = hash(pId);
  return nameHash.slice(0,6) +pHash.slice(0,6)+pIdHash.slice(0,6)+addressIdHash.slice(0,13)+addressIdHash2.slice(0,13)+addressIdHash3.slice(0,13)+addressIdHash4.slice(0,13);

}



/* 
function to register product to dealer

mkey - manufacturer key
productR - product name
productId - the unique product Id given for each product
Dom - Date of manufacture
Dod - Date of Distribution
DName - Dealers name
DId - Dealers unique idetification number
*/ 

  addToRegisterM(manufacturer,mkey,productR,productId,Dom,Dod,DName,DId){
    console.log("**********client");
    console.log("mkey",mkey);
    console.log("product:",productR);
    console.log("id",productId);
    console.log("dom",Dom);
    console.log("dom",Dod);
    console.log("dname",DName);
    console.log("did:", DId);
    console.log("************************");


    let action= "Add product";
    console.log("action",action);
    let address= this.getProductAddress(productR,productId);
    console.log("address",address);
    console.log("address",address.length);
  
    let payload = [action,manufacturer,productR,productId,Dom,Dod,DName,DId].join(',');
    
    createTransaction(FAMILY_NAME,[address],[address],mkey,payload);

  }

/*
function to remove product by the manufacturer'
mkey - manufacturer key
productR - product name
productId - the unique product Id given for each product

*/ 
  removeProduct(manufacturer,mkey,productR,productId){
    console.log("**********client");
    console.log("mkey",mkey);
    console.log("product:",productR);
    console.log("id",productId);

    let action= "Remove product";
    console.log("action",action);
    let address= this.getProductAddress(productR,productId);
    console.log("address",address);
    console.log("address",address.length);

    let payload = [action,manufacturer,productR,productId].join(',');

    createTransaction(FAMILY_NAME,[address],[address],mkey,payload);
  }

  /* 
function to register product to customer

dKey - dealer key
productR - product name
pId - the unique product Id given for each product
cName -  the customer name to who the product is sold to
cMail - customer mail
cPhone - customer phone number 
dos - date of sale of product to customer
*/ 
  addToRegisterD(dealer,dKey,productR,pId,cName,cMail,cPhone,dos){
    
    console.log("**********client dealer");
    console.log("dkey",dKey);
    console.log("dkey",dKey.length);
    console.log("product:",productR);
    console.log("id",pId);
    console.log("cname",cName);
    console.log("cmail",cMail);
    console.log("cPhone",cPhone);
    console.log("dos:", dos);
    console.log("************************");

    let address= this.getProductAddress(productR,pId);

      console.log("address",address);
    console.log("address",address.length);

    let action= "Add customer";
    let payload = [action,productR,pId,cName,cMail,cPhone,dos,dealer].join(',');

      createTransaction(FAMILY_NAME,[address],[address],dKey,payload);
  
  }
//////

/**
 * Get state from the REST API
 * @param {*} address The state address to get
 * @param {*} isQuery Is this an address space query or full address
 */
async getState (address, isQuery) {
  let stateRequest = 'http://rest-api:8008/state';
  if(address) {
    if(isQuery) {
      stateRequest += ('?address=')
    } else {
      stateRequest += ('/address/');
    }
    stateRequest += address;
  }
  let stateResponse = await fetch(stateRequest);
  let stateJSON = await stateResponse.json();
  return stateJSON;
}

async getProductListings() {
  let ProductListingAddress = hash(FAMILY_NAME).substr(0, 6);
  return this.getState(ProductListingAddress, true);
}

////
}

module.exports = { Product };

  

  
