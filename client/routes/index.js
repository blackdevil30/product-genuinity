var express = require('express');
var { Product } = require('./usersClient');
var router = express.Router();


/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Product Genuinity' });
});

/* Get manufactures dashboard*/ 
router.get('/Manu',(req,res,next)=>{
  res.render('ManuLogin');
});

/* Get Dealers dashboard*/ 
router.get('/Deal',(req,res,next)=>{
  res.render('DealerLogin');
});

router.get('/rem', (req,res)=> {
  res.render('deletepro');
})

/*To get table view of all the products for the manufacturer*/ 
router.get('/mainView', async(req,res,next)=> {
var productObj =new Product();
let stateData= await productObj.getProductListings();
let productList = [];
stateData.data.forEach(productsR=> {
  if(!productsR.data) return;
  let decodedProducts = Buffer.from(productsR.data, 'base64').toString();
  let productDetails = decodedProducts.split(',');
  // for(let i=0; i< productDetails.length; i++ ){
  //   console.log("product-list:", productDetails[i]);
  // }
  productList.push({
    product: productDetails[1],
    productId: productDetails[2],
    dateOfManufacture: productDetails[3],
    dateOfDistribution: productDetails[4],
    dealerName: productDetails[5],
    dealerId: productDetails[6],
    status: (productDetails.length === 7) ?"still Pending" : "sold",
    customerName: productDetails[7],
    customerMail: productDetails[8],
    customerPhoneNum: productDetails[9],
    DateOfSale: productDetails[10]
     

  });
  
});

// for(var i=0;i< productList.length; i++) {
//   console.log(productList[i], typeof(productList[i]));
// }
res.render('mainView',{ listings: productList});
});



/*To get table view of all the products for the Dealer*/ 
router.get('/DealView', async(req,res,next)=> {
  var productObj =new Product();
  let stateData= await productObj.getProductListings();
  let productList = [];
  stateData.data.forEach(productsR=> {
    if(!productsR.data) return;
    let decodedProducts = Buffer.from(productsR.data, 'base64').toString();
    let productDetails = decodedProducts.split(',');
    // for(let i=0; i< productDetails.length; i++ ){
    //   console.log("product-list:", productDetails[i]);
    // }
    productList.push({
      product: productDetails[1],
      productId: productDetails[2],
      dateOfManufacture: productDetails[3],
      dateOfDistribution: productDetails[4],
      dealerName: productDetails[5],
      dealerId: productDetails[6],
      status: (productDetails.length === 7) ?"still Pending" : "sold",
      customerName: productDetails[7],
      customerMail: productDetails[8],
      customerPhoneNum: productDetails[9],
      DateOfSale: productDetails[10]
       
  
    });
    
  });
  

  res.render('DealView',{ listings: productList});
  });



/* To get view for single product in the home page */ 
router.get('/view', async(req,res,next)=> {
 
  var productObj = new Product();
  let stateData= await productObj.getProductListings();
  let productList = [];


  stateData.data.forEach(productsR=> {
    if(!productsR.data) return;
    
    let decodedProducts = Buffer.from(productsR.data, 'base64').toString();
    let productDetails = decodedProducts.split(',');
    // for(let i=0; i< productDetails.length; i++ ){
    //   console.log("product-list:", productDetails[i]);
    // }
      productList.push({
      product: productDetails[1],
      productId: productDetails[2],
      dateOfManufacture: productDetails[3],
      dateOfDistribution: productDetails[4],
      dealerName: productDetails[5],
      dealerId: productDetails[6],
      status: (productDetails.length === 7) ?"still Pending" : "sold",
      customerName: productDetails[7],
      customerMail: productDetails[8],
      customerPhoneNum: productDetails[9],
      DateOfSale: productDetails[10]
    });
  
  });
  
  res.render('view', {listings: productList});
});



/*
 posting the elements to the client: by Manufacturer for Dealer Registration 
mkey- manufactures private key
productR - product name
pId - unique product Id generated
dom - date of Manufacture
dod - date of Distribution
dname - Dealer Name
dId - unique dealer ID

*/
router.post('/addToD',(req,res) => {
  let mKey= req.body.mKey;
  let productR =req.body.product;
  let pId = req.body.pId;
  let dom = req.body.dom;
  let dod = req.body.dod;
  let dname= req.body.dName;
  let dId = req.body.dId;
  
  console.log("********index.js******");
  console.log("mkey",mKey);
    console.log("product:",productR);
    console.log("id",pId);
    console.log("dom",dom);
    console.log("dom",dod);
    console.log("dname",dname);
    console.log("did:", dId);
  console.log("************");

    console.log("data sent to the client");
    var client = new Product();
    client.addToRegisterM("Manufacturer", mKey,productR,pId,dom,dod,dname,dId);
    res.send({message:  "data added successfully"});

});

/*
posting the eloements to the client by manufacture to remove a product
mkey- manufactures private key
productR - product name
pId - unique product Id generated
*/ 
router.post('/rempro',(req,res, next) => {
  let mKey= req.body.mKey;
  let productR =req.body.product;
  let pId = req.body.pId;

  console.log("********index.js****remove**");
  console.log("mkey",mKey);
  console.log("product:",productR);
  console.log("id",pId);
  console.log("*************");
  
  var client = new Product();
  client.removeProduct("Manufacturer", mKey,productR,pId);
  res.send({message:  "data added successfully"});
})


/*
 posting the elements to the client: by Dealer for Customer Registration 
dkey- manufactures private key
productR - product name
pId - unique product Id generated
cName - customer Name
cPhone- customer Phone Number
cMail - customer mail
dos - date of sale
*/

router.post('/addToC', (req,res)=> {

  let dKey = req.body.dKey;
  let productR =req.body.product;
  let pId = req.body.pId;
  let cName =req.body.cName;
  let cPhone = req.body.cPhone;
  let cMail = req.body.cMail; 
  let dos = req.body.dos;

  console.log("********index.js******");
    console.log("dkey:", dKey);
    console.log("product:",productR);
    console.log("id",pId);
    console.log("cname:", cName);
    console.log("cmail:", cMail);
    console.log("cPhone:", cPhone);
    console.log("dos",dos);
    console.log("************");

    var client = new Product();
    client.addToRegisterD("Dealer",dKey,productR,pId,cName,cMail,cPhone,dos);
    res.send({message:  "data added successfully"});

})



module.exports = router;
