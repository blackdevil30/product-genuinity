/*
function to generate unique product identification number
family - just a family name for all the product here PG(product geneunity)
pro - product name(eg:laptop) 
pFamily - product family (eg:pivilion15)
model - modelnum of the product(ay011)
deviceNum - the device production number must beunique for each product

*/ 

function proId(){
   
    const family = "pg";
    var pro = document.getElementById("pR").value.trim();
    var pFamily = document.getElementById("pfamily").value.trim();
    var model = document.getElementById("model").value.trim();
    var deviceNum = document.getElementById("divNum").value.trim();

   

    console.log(pro);
    console.log(pFamily);
    console.log(model);
    console.log(deviceNum);
    
    
    var x =  family+pro.slice(0,4)+pFamily.slice(0,3)+model.slice(0,3)+deviceNum;
    return x;
}




// function to add product deatils and validate by the Manufacturer

function addToRegisterM(event){
    event.preventDefault();
    let mKey = document.getElementById('manuKey').value.trim();
    let product = document.getElementById('pR').value.trim();
    let pId= document.getElementById('pId').value.trim();
    let dom = document.getElementById('dom').value.trim();
    let dod = document.getElementById('dod').value.trim();
    let dname= document.getElementById('dName').value.trim();
    let dId = document.getElementById('dId').value.trim();

    let domUp = new Date(dom.replace(/-/g,'/'));
    let dodUp = new Date(dod.replace(/-/g,'/'));

    // console.log("mkey",mKey);
    // console.log("product:",product);
  

    if(mKey.length == '' || product.length == '' || pId.length == '' || dom.value == '' || dod.value == '' || dname.length == '' ||  dId.length == ''){
        alert("Please check the Data Provided!!!");
    }
    else if (!product.match(/^[0-9a-zA-Z]+$/)){
        alert("please provide correct product name");
    }
   

    else if(!dom.match(/^(\d{4})[- /.](0?[1-9]|1[012])[- /.](0?[1-9]|[12][0-9]|3[01])*$/)) {
        alert("please check the manufacture Date");
    }
    else if(!dod.match(/^(\d{4})[- /.](0?[1-9]|1[012])[- /.](0?[1-9]|[12][0-9]|3[01])*$/)) {
        alert("please check the distribution Date");
    }

    else if(domUp > dodUp) {
        alert("Please check the manufacture and the distribution dates");
    }
    else if(!dname.match(/^[a-zA-Z0-9]+$/)){
        alert("provide proper dealer name");
    } 

    else if(!dId.match(/^[0-9a-zA-Z]+$/)){
        alert("please provie proper product Id");

    }
    else {
        $.post('/addToD', {mKey: mKey, product: product, pId: pId, dom: dom, dod: dod, dName: dname, dId: dId},'json');
        alert("product is added!!");
        document.getElementById("manuForm").reset();
        return true;
    }

}


// function to delete the product by a manufacturer
function DeletePro(event){
    event.preventDefault();
    let mKey = document.getElementById('manuKey').value.trim();
    let product = document.getElementById('pR').value.trim();
    let pId= document.getElementById('pId').value.trim();

    if(mKey == ''|| product == '' || pId == ''){
        alert("please fill the data");
    }
    else{
        if(confirm("Do you want to Remove product "+"\n"+product+" : "+pId)){
        $.post('/rempro',{mKey: mKey, product: product, pId: pId});
         document.getElementById("manuForm").reset();
         return true;
        }
        return;
    }

}





// function to validate and send data by the dealer
function addToRegisterD(event){
    event.preventDefault();

    let dKey = document.getElementById('dealKey').value.trim();
    let product= document.getElementById('pR').value.trim();
    let pId = document.getElementById('pId').value.trim();
    let cName =document.getElementById('cName').value.trim();
    let cMail = document.getElementById('cMail').value.trim();
    let cPhone = document.getElementById('cPhone').value.trim();
    let dos = document.getElementById('dos').value.trim();

    // console.log("dkey:", dKey);
    // console.log("product:",product);


    if(dKey == '' || product == '' || pId == '' || cName == '' || cMail == '' || cPhone == ''|| dos == ''){
        alert("Please check the Data Provided!!!");

    }
    else if (!product.match(/^[0-9a-zA-Z]+$/)){
        alert("please provide correct product name");
    }
  
    else if(!cName.match(/^[a-zA-z]+$/)){
        alert("please check the customer name");

    }

    else if(!cPhone.match(/^[1-9][0-9]{9}$/)){
        alert("enter the phone number correctly");
    }
    else if(!dos.match(/^(\d{4})[- /.](0?[1-9]|1[012])[- /.](0?[1-9]|[12][0-9]|3[01])*$/)){
        alert("enter the proper date!!");

    }
    else if(!cMail.match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/)){
        alert("enter the proper email");
    }
    else {
        $.post('/addToC', {dKey: dKey, product: product, pId: pId, cName: cName, cMail: cMail,cPhone: cPhone, dos: dos},'json');
        alert("product is added!!");
        document.getElementById("dealForm").reset();
        return true;
    
    }
}

