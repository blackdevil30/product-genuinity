#Product Genuinity Register

##Overview
Whenever we had to buy a product even an electronic product or a banded clothing’s, online or offline the question often comes to our mind is whether the product is genuine one, or it’s just a duplicate one what if it’s a fraudster. As a solution to ensure the products authenticity here is a distributed ledger which can be used by the manufacturer and dealer to record data so that the customer can check in to its ledger and see whether the product exists. This ledger can be also used by the manufacturer to track the product to find the market of the product if the product has demand or not and ensure the warranty or the guarantee acquired by the customer.

##Description
Product Genuinity is a proof-of-concept implementation for efficient and effective tamper proof recording of details of the product. It’s a distributed ledger that allows the manufacture and the dealer to record product data and a customer can check it to ensure whether the product is genuine or not. This ledger can also be helpful in tracking a product as well. Three Users mainly manufacturer, dealer, and customer is involved.

##Components
1. Client: the client application is developed using Express & Node js, with handlebars templates for the webpages.
2. Transaction Processor: Transaction from both Manufactures side and Dealers side are handled by the transaction processor


##Users
Manufacturer: The manufacturer can register, remove and check the product from the ledger. The manufacture also records the dealer’s details
Dealer:  The dealer mainly records customer data in to the ledger and can view the product as well.
Customer: The customer can check the authenticity of the product by providing the product Id.


### System requirements:

1. Operating system: Ubuntu 16.04
2. System RAM: 4 GB or above (recommended 8 GB)
3. Free System storage: 4 GB on /home

### Installation prequisites:

1. Ensure that NodeJS (version 10.16.3 was used during development) is installed in the system. For more information about NodeJS, go to https://nodejs.org. To check if installed, open a terminal window:

   ```$ node -v```

2. If NodeJS is not installed, go to https://nodejs.org and download the compatible version (version) based on system OS, or in a terminal window:

   ```$ sudo apt-get install -y nodejs```

3. Ensure that Docker is installed. Docker is a skimmed down version of Virtual Machine which overcomes some limitations of VM. For more information, go to https://www.docker.com/resources/what-container. To check if installed, in terminal window:

```$ sudo docker --version```

4. If Docker is not installed, in terminal window:

    **Set up the repository**
*   Update the apt package index:

    ```$ sudo apt-get update```
*   Install packages to allow apt to use a repository over HTTPS:

    ```
    $ sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
    ```
*   Add Docker’s official GPG key:

    ```$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -```
*   Use the following command to set up the stable repository.
    ```
    $ sudo add-apt-repository \
    "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
    $(lsb_release -cs) \
    stable"
    ```
    **Install Docker CE**
*   Update the apt package index.

    ```$ sudo apt-get update```
*   Install the latest version of Docker CE.

    ```$ sudo apt-get install docker-ce```
*   Verify that Docker CE is installed correctly by running the hello-world image.

    ```$ sudo docker run hello-world```
    
   This command downloads a test image and runs it in a container. When the container runs, it prints an informational message and exits.

5. Ensure that Docker Compose is installed. Compose is a tool for defining and running multi-container Docker applications. To check if installed, in terminal window:

   ```$ sudo docker-compose --version```
6. If Docker Compose is not installed, in terminal window:

   ```
   $ sudo apt-get update
   $ sudo apt-get install docker-compose
   ```
### Installation/Set-up instructions:



1. Run the project YAML file using Docker Compose (docker-compose.yaml in Project folder). Open a terminal window in the project folder:

   ```$ sudo docker-compose up```

Compose pulls and builds an image for the code, and starts the services defined in it.

2. Since this involves participation of public, setting up permissioning will be a repeatative and tedious work.

3. Note : Permissioning can be done as follows:

+ Open a new tab in terminal by using `CTRL+SHIFT+T` , and log into the validator 

    ```sudo docker exec -it validator bash```

+ **Creating new keys** : For the current project, three places are considered. So total of 2 keys are generated(Enter one by one).These 2 keys are mandatory

```sawtooth keygen manufacturer```
```sawtooth keygen dealer``` 


+ Now the keys are generated. For viewing the private and public keys : 

```cat ~/.sawtooth/keys/manufacturer.priv``` 
```cat ~/.sawtooth/keys/manufacturer.pub``` 
```cat ~/.sawtooth/keys/dealer.priv``` 
```cat ~/.sawtooth/keys/dealer.pub``` 

+ After creating the keys, for configuring policy and setting roles, we are making the my_key as an **allowed key** : 
    
    ```sawset proposal create --key ~/.sawtooth/keys/my_key.priv sawtooth.identity.allowed_keys=$(cat ~/.sawtooth/keys/my_key.pub) --url http://rest-api:8008```

+ For listing the settings :

    ```sawtooth settings list --url http://rest-api:8008 --format json```

+ For creating a proposal for policy : 

    ```sawtooth identity policy create --key ~/.sawtooth/keys/my_key.priv policy_pg "PERMIT_KEY $(cat /root/.sawtooth/keys/my_key.pub)" "PERMIT_KEY $(cat /root/.sawtooth/keys/manufacturer.pub)" "PERMIT_KEY $(cat /root/.sawtooth/keys/dealer.pub)" --url http://rest-api:8008```

+ For viewing the policy list :
  ```sawtooth identity policy list --url http://rest-api:8008 --format json```

+ For assigning transactor role to the current policy file : 

    ```sawtooth identity role create --key ~/.sawtooth/keys/my_key.priv transactor.transaction_signer policy_pg --url http://rest-api:8008```

+ For view role of the current policy:
	```sawtooth identity role list --url http://rest-api:8008 --format json```


+ **Permissioning Completed**

4. Open browser and go to http://localhost:3000

5. To terminate the app execution, go to the terminal window (where docker-compose is running) and give `CTRL+C`

6. Wait for docker-compose to gracefully stop. Then:

    ```$ sudo docker-compose down```

### Project conditions:

+ For demonstrating the working of the project any two private key is  required one for manufacturer and dealer.

+ The manufacturer can record, remove data with the private key. 

+ The dealer can record data with the private key

+ customer can just check the product without any restrictions currently the check product part for the manufacturer and dealer is open as a part of future extension user authentication can be included.



##### Created by: 
##### Nithin jayaprakash
##### GitLab: blackdevil30

































