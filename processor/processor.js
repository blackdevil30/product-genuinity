const { TransactionProcessor } = require('sawtooth-sdk/processor');
const Product = require('./handler');
const URL = 'tcp://validator:4004';

const transactionProcesssor = new TransactionProcessor(URL);
transactionProcesssor.addHandler(new Product());
transactionProcesssor.start();