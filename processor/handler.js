/* Transaction Processor */
const {TextDecoder,TextEncoder} = require('text-encoding/lib/encoding');
const { TransactionHandler } = require('sawtooth-sdk/processor/handler');
const crypto = require('crypto');


const FAMILY_NAME = "product genuinity";
const NAMESPACE = hash(FAMILY_NAME).substring(0, 6);
var decoder = new TextDecoder('utf8');
var encoder = new TextEncoder('utf8');


//function to create hash 
function hash(data) {
    return crypto.createHash('sha512').update(data).digest('hex');
}


// function to display the errors
var _toInternalError = function (err) {
    console.log(" in error message block");
    var message = err.message ? err.message : err;
    throw new InternalError(message);
  };


/* function to write data to state 
parameter : 
    context -  validator context object
    address - address to which data should be written to
    data - the data tto be written
*/
function writeToStore(context, address, data){
        dataBytes = encoder.encode(data)
        let entries = {
        [address]: dataBytes
      }
    return context.setState(entries);
    
}

/*
function to remove product by the manufacturer'
context - validator context object
address - the address to which data has to be stored in the state accuired from the header-inputlist of the transaction
*/ 

function deleteFromStore(context, address){
   
    return context.deleteState([address]);
}





/* 
function to pass register deatil to state store 
and trigger custom events and store transaction receipt data

context - validator context object
address - the address to which data has to be stored in the state accuired from the header-inputlist of the transaction
productR - product name
productId - the unique product Id given for each product
dom - Date of manufacture
dod - Date of Distribution
dName - Dealers name
dId - Dealers unique idetification number

*/

function addRegistrationMan(context,address,manufacturer,productR,productId,dom,dod,dName,dId){
    console.log("******************handler");
 
    console.log("product:",productR);
    console.log("id",productId);
    console.log("dom",dom);
    console.log("dom",dod);
    console.log("dname",dName);
    console.log("did:", dId);

    // let productAddress = getProductAddress(productR,productId);
    let productAddress = address;
    let productDetail = [manufacturer,productR,productId,dom,dod,dName,dId];

    console.log("added custom event*************Regsitering the Dealer***************");
    context.addEvent('product genuinity/ProductRegisteredToDealer',
    [['product',productR],
    ['productId', productId],
     ['Date of Manufacture', dom], 
     ['Date of distribution', dod], 
     ['Dealer', dName],
     ['Dealer id', dId]],null);
     
     
    context.addReceiptData(Buffer.from("product " +productId+" has been sold to: "+dName+" and registered in the address: "+productAddress));
    console.log('***Transaction receipt data created*****');

    console.log("Data sent to write in state!!");
    return writeToStore(context,productAddress,productDetail);
}


/*
function to remove product by the manufacturer'
context - validator context object
address - the address to which data has to be stored in the state accuired from the header-inputlist of the transaction
product - product name
productId - the unique product Id given for each product

*/ 
function removeProduct(context,address,product,productId){
console.log("**************");
console.log("product:",product);
console.log("id",productId);

let paddress = address;
console.log("address:",address);

context.addEvent('product genuinity/ProductDeleted',
[['product',product],
['productId', productId]],null);
console.log("******custom event*******");

context.addReceiptData(Buffer.from("product "+product+" : "+productId+"in the address :"+paddress+"has been deleted"));
console.log("******Transaction receipt added*******");

return deleteFromStore(context,paddress);

}




/* 
function to register product deatils by a dealer about customer
and trigger custom event and store receipt data

context - validator context object
address - the address to which data has to be stored in the state accuired from the header-inputlist of the transaction
productR - product name
productId - the unique product Id given for each product
cName -  the customer name to who the product is sold to
cMail - customer mail
cPhone - customer phone number 
dos - date of sale of product to customer
*/
function completeRegistrationDealer(context,address,productR,productId,cName,cMail,cPhone,dos,dealer){


console.log("Registering A Product to customer");
console.log("******************handler");
 
    console.log("product:",productR);
    console.log("id",productId);
    console.log("dos",dos);
    console.log("cname:", cName );
    console.log("cmail:", cMail);
    console.log("cPhone:", cPhone);

let pAddress = address;
return context.getState([pAddress]).then(function(data){
    console.log("data",data);
    if(data[pAddress] == null || data[pAddress] == "" || data[pAddress] == []){
        console.log("Invalid product!");
    }else{
    let stateJSON = decoder.decode(data[pAddress]);
    let newData = stateJSON + "," + [cName,cMail,cPhone,dos,dealer].join(',');
   
    console.log("added custom event *******Registeration customer*********************");

    context.addEvent('product genuinity/ProductRegisteredToCustomer',
    [['product',productR],
    ['productId', productId],
    ['Customer name', cName],
    ['customer mail', cMail],
    ['customer Phone', cPhone],
    ['Date of sale', dos]],null);

    context.addReceiptData(Buffer.from("product " +productId+" has been sold to: "+cName+" and registered in the address: "+pAddress));
    console.log('***Transaction receipt data created*****');
    console.log("Data stored in state!!");

    return writeToStore(context,pAddress,newData);
    
  
    
    }
    });
}
  
  
  
  
  
  
  class Product extends TransactionHandler{
      constructor(){
          super(FAMILY_NAME, ['1.0'], [NAMESPACE]);
      }


    //   apply function 
            apply(transactionProcessRequest, context){

            try{
                

                 let PayloadBytes = decoder.decode(transactionProcessRequest.payload);
              
                let Payload = PayloadBytes.toString().split(',');
               
                for(var i=0;i < Payload.length; i++)
                {
                    console.log("payload"+i+":",Payload[i]);
                }

                let header = transactionProcessRequest.header;
                let address = header.inputs;
                console.log("address*******",address);
                let addressStr = address.toString();
                console.log("address*******",addressStr);
                
                let action = Payload[0];
                if (action === "Add product"){
                    return addRegistrationMan(context,addressStr,Payload[1],Payload[2],Payload[3],Payload[4],Payload[5],Payload[6],Payload[7]);

                }
                else if(action === "Add customer"){
                    return completeRegistrationDealer(context,addressStr,Payload[1],Payload[2],Payload[3],Payload[4],Payload[5],Payload[6],Payload[7]);
                }
                else if(action === "Remove product"){
                    return removeProduct(context,addressStr,Payload[2],Payload[3]);
                }
                
            }
            catch(err){

                console.log("Error inside apply function of CFTHandler : ",err);
                _toInternalError(err);
          
              }

        }
  }

  module.exports = Product;